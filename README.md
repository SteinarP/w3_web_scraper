Run "node scraper.js", or "npm start".

Method topOfSubreddit(<subreddit>) saves a screenshot of today's top posts of reddit.com/r/<subreddit>.

Method getWikiLinks(<topic>) will collect all links to other wiki pages on en.wikipedia.org/wiki/<topic>.
These links are written as JSON to ./wikilinks/<topic>.json