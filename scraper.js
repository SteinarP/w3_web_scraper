const puppeteer = require("puppeteer");
const jurl = require("url");
const fs = require('fs');
const cheerio = require("cheerio");


const ensureDir = dir => {if (!fs.existsSync(dir)) fs.mkdirSync(dir)};

//Any smarter way to make this just fill in what's missing without chains of ifs?
//const toUrl = s => s.startsWith("https://www.") ? s : "https://www."+s; 
const now = () => new Date().toISOString().split(/[T.]/).slice(0,2).join("_"); //date, time, not ms
const today = () => new Date().toISOString().split("T")[0];

async function topOfSubreddit(subreddit){
    url = "https://www.reddit.com/r/"+subreddit+"/top/?t=day";
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto(url);

    ensureDir("./reddit");
    const ssPath = "./reddit/"+subreddit+"_"+today()+".png";
    await page.screenshot({path: ssPath, fullPage: true});

    await browser.close();
    console.log("Done: Saved screenshot to "+ssPath);
}
//Note: Practise function. Doesn't use json. See getWikiLinks();
topOfSubreddit("programmerhumor");
topOfSubreddit("javascript");


const wikiLinkToTitle = path => decodeURI(path.split("#")[0].replace("/wiki/","").split("_").join(" "));
async function getWikiLinks(topic, log=false){
    const url = "https://en.wikipedia.org/wiki/"+topic;
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto(url);
    const bodyHTML = await page.evaluate(() => document.body.innerHTML);
    const $ = cheerio.load(bodyHTML);

    let links = [];
    $(".mw-parser-output > p > a").map((_,elem) => {
        const href = $(elem).attr("href").trim().split("#")[0]; //#: Trim off wiki section
        const linkText = $(elem).contents().first().text().trim();
        //console.log("Adding\t",wikiSpaced(href));
        links.push({href, linkText});
    })

   //https://stackoverflow.com/questions/15125920/how-to-get-distinct-values-from-an-array-of-objects-in-javascript
    //Keep only unique links
   const unique_links = [...new Map(links.map(a =>
        [a["href"], a])).values()];
    if (log) console.log("Done. Found",unique_links.length,"unique links.");
    //Keep only links to wikipedia.org/wiki/
    const only_wiki = unique_links.filter(function(item) {
        return item.href.startsWith("/wiki/");
      });
    //Sort wiki pages alphabetically
    const sorted = only_wiki.sort(function(a, b) {
        var textA = a.href.toUpperCase();
        var textB = b.href.toUpperCase();
        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });

    /*

    complete.forEach(e => {
        console.log(wikiLinkToTitle(e.href));
    })*/

    //Scraper is naively hardcoded for English wikipedia
    const base = "https://en.wikipedia.org"; //Has "/wiki/topic"
    //const complete = sorted.map(el => (base + el.href, el.linkText ));

    let complete = []
    sorted.forEach(el => {
        linkObj = { href: base + el.href,
                    linkText: el.linkText
                }; //Any easier way to do this? Map?
        if (log) console.log(linkObj.href);
        complete.push(linkObj);
    })

    let data = JSON.stringify(complete, null, 2); //ANewline, indents
    ensureDir("./wikilinks");
    let writePath = "./wikilinks/"+topic+".json";
    fs.writeFileSync(writePath, data);
    console.log("Done: Wrote ",complete.length,"links to",writePath)
    return complete;
}

//These are the ones using JSON
getWikiLinks("JavaScript");
getWikiLinks("Norway");
getWikiLinks("Noroff", true);
getWikiLinks("ManpowerGroup", true);

//process.exit(0);